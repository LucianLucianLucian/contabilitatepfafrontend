//Because a model is a class, to generate it use --type option like this:
//
// ng generate class hero --type=model
// will result in:
//
// hero.model.ts
import {Cumparator} from "./cumparator";

export class Factura {
  constructor() {
  }

  id: number;
  serie: string;
  total: bigint;
  dataCrearii: string;


}
