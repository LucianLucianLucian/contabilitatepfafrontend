import {Factura} from "./factura.model";

export class Cumparator {

  id: number;
  nume: string;
  cif: string;
  cui: string;
  judetul: number;
  factura: Factura[];

}
