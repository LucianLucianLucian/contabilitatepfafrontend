import {Component, OnInit, Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Factura} from "../model/factura.model";
import {FacturaApiService} from '../service/factura.api.service'
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.scss']
})

export class FacturaComponent implements OnInit {
  facturi: any;

  constructor(private router: Router, private apiService: FacturaApiService) {
  }

  tableColumns: string[] = ['id', 'serie', 'data', 'total', 'nume'];

  ngOnInit() {
    this.apiService.getFacturi()
      .subscribe(data => {
        // @ts-ignore
        this.facturi = new MatTableDataSource(data);
      });
  }

  applyFilter(filterValue: string) {
    this.facturi.filter = filterValue.trim().toLowerCase();
  }

  addFactura(): void {
    this.router.navigate(['adaugafactura']);
  };
}
