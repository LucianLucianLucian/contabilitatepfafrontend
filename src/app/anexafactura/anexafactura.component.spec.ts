import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnexafacturaComponent } from './anexafactura.component';

describe('AnexafacturaComponent', () => {
  let component: AnexafacturaComponent;
  let fixture: ComponentFixture<AnexafacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnexafacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnexafacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
