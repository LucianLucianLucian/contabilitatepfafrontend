import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdaugafacturaComponent } from './adaugafactura.component';

describe('AdaugafacturaComponent', () => {
  let component: AdaugafacturaComponent;
  let fixture: ComponentFixture<AdaugafacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdaugafacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdaugafacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
