import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {FacturaApiService} from '../service/factura.api.service'
import {CumparatorApiService} from "../service/cumparator.api.service";
import {Cumparator} from "../model/cumparator";
import {Factura} from '../model/factura.model';
import {MatDialog} from '@angular/material';
import {AdaugaClientComponent} from "../adauga-client/adauga-client.component";

@Component({
  selector: 'app-adaugafactura',
  templateUrl: './adaugafactura.component.html',
  styleUrls: ['./adaugafactura.component.scss']
})
export class AdaugafacturaComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private facturaApiService: FacturaApiService
    , private cumparatorApiService: CumparatorApiService, public dialog: MatDialog) {
  }

  addForm: FormGroup;
  cumparatori: any;
  selectedValue: Cumparator;


  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      serie: [],
      total: [],
      dataCrearii: []
    });

    this.cumparatorApiService.getCumparatori()
      .subscribe(data => {
        console.log(data);
        // @ts-ignore
        this.cumparatori = data;
      });

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AdaugaClientComponent, {
      width: '250px'/*,
      data: {name: this.name, animal: this.animal}*/
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onSubmit() {
    this.selectedValue.factura = this.addForm.value;
    console.log(this.selectedValue);
    this.cumparatorApiService.updateCumparator(this.selectedValue).subscribe(data => {
      this.router.navigate(['factura']);
    })
  }

  addClient() {
    this.router.navigate(['adauga-client']);
  }

  adaugaAnexa(){
    this.router.navigate(['anexafactura']);

  }

}
