import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdaugaClientComponent } from './adauga-client.component';

describe('AdaugaClientComponent', () => {
  let component: AdaugaClientComponent;
  let fixture: ComponentFixture<AdaugaClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdaugaClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdaugaClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
