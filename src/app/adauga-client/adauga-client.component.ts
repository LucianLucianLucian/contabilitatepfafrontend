import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Cumparator} from "../model/cumparator";
import {MatDialogModule} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {FacturaApiService} from '../service/factura.api.service'
import {CumparatorApiService} from "../service/cumparator.api.service";
import {Factura} from '../model/factura.model';
import {JudetApiService} from '../service/judet.api.service';

@Component({
  selector: 'app-adauga-client',
  templateUrl: './adauga-client.component.html',
  styleUrls: ['./adauga-client.component.scss']
})
export class AdaugaClientComponent implements OnInit {
  cumparator: Cumparator = new Cumparator();

  constructor(
    public dialogRef: MatDialogRef<AdaugaClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cumparator,
    private judetApiService: JudetApiService, private cumparatorApiService: CumparatorApiService, private router: Router) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveCumparator() {
    this.cumparatorApiService.createCumparator(this.cumparator).subscribe(data => {
      this.onNoClick();
      window.location.reload();
    });
  }

  ngOnInit() {
    this.judetApiService.getJudete()
      .subscribe(data => {
        // @ts-ignore
        this.judete = data;
      });
  }

}
