import { TestBed } from '@angular/core/testing';

import { Factura.ApiService } from './factura.api.service';

describe('Factura.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Factura.ApiService = TestBed.get(Factura.ApiService);
    expect(service).toBeTruthy();
  });
});
