import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Factura} from "../model/factura.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api-response.model";

@Injectable()
export class FacturaApiService {

  constructor(private http: HttpClient) {
  }

  baseUrl: string = 'http://localhost:8080/factura/all';
  adaugaFactura: string = 'http://localhost:8080/factura';

  getFacturi(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  // getUserById(id: number): Observable<ApiResponse> {
  //   return this.http.get<ApiResponse>(this.baseUrl + id);
  // }

  createFactura(factura: Factura): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.adaugaFactura, factura);
  }

  // updateUser(user: User): Observable<ApiResponse> {
  //   return this.http.put<ApiResponse>(this.baseUrl + user.id, user);
  // }
  //
  // deleteUser(id: number): Observable<ApiResponse> {
  //   return this.http.delete<ApiResponse>(this.baseUrl + id);
  // }
}
