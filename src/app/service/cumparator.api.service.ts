import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api-response.model";
import {Cumparator} from '../model/cumparator';
import {Factura} from "../model/factura.model";

@Injectable()
export class CumparatorApiService {

  constructor(private http: HttpClient) {
  }

  getAllCumparatori: string = 'http://localhost:8080/cumparator/all';
  getCumparatorByIdentifier: string = 'http://localhost:8080/cumparator/';
  updateURL: string = 'http://localhost:8080/cumparator/update';
  postURL: string = 'http://localhost:8080/cumparator';

  getCumparatori(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.getAllCumparatori);
  }

  getCumparatorById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.getCumparatorByIdentifier + id);
  }

  updateCumparator(cumparator: Cumparator): Observable<ApiResponse> {
    return this.http.patch<ApiResponse>(this.updateURL, cumparator);
  }

  createCumparator(cumparator: Cumparator): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.postURL, cumparator);
  }
}
