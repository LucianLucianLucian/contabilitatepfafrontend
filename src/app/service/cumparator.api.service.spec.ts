import { TestBed } from '@angular/core/testing';

import { CumparatorApiService } from './cumparator.api.service';

describe('Cumparator.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CumparatorApiService = TestBed.get(CumparatorApiService);
    expect(service).toBeTruthy();
  });
});
