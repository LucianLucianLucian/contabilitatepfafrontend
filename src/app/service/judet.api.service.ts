import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api-response.model";
import {Cumparator} from '../model/cumparator';
import {Factura} from "../model/factura.model";

@Injectable({
  providedIn: 'root'
})
export class JudetApiService {

  constructor(private http: HttpClient) {
  }


getAllJudete: string = 'http://localhost:8080/judet/all';

  getJudete(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.getAllJudete);
  }


}
