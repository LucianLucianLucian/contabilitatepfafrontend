import {TestBed} from '@angular/core/testing';

import {JudetApiService} from './judet.api.service';
import {Judet} from '../model/judet.enum';

describe('Judet.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JudetApiService = TestBed.get(JudetApiService);
    expect(service).toBeTruthy();
  });
});
